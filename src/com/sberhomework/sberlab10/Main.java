package com.sberhomework.sberlab10;

import com.sberhomework.sberlab10.threading.FixedThreadPool;
import com.sberhomework.sberlab10.threading.MyThreadPool;
import com.sberhomework.sberlab10.threading.ScalableThreadPool;

public class Main {

    private static Runnable getTask(String taskName, int waitTime){
        return () -> {
            try {
                System.out.println(Thread.currentThread().getName()+" '"+taskName+ "': Task Started");
                Thread.sleep(waitTime);
                System.out.println(Thread.currentThread().getName() +" '"+taskName+ "': Task Completed");
            } catch (InterruptedException e) {
                System.out.println(Thread.currentThread().getName() +" '"+taskName+ "': Interrupted Exception");
            }
        };
    }

    public static void fixedThreadPoolDemo(){
        System.out.println("\nFixed Thread Pool Test:");
        MyThreadPool threadPool = new FixedThreadPool(2);
        threadPool.execute(getTask("FTask1",1000));
        threadPool.execute(getTask("FTask2",500));
        threadPool.execute(getTask("FTask3",200));
        threadPool.shutdown();

        threadPool.submit(getTask("FTask4",100));
        threadPool.submit(getTask("FTask5",100));
        threadPool.submit(getTask("FTask6",100));
        threadPool.submit(getTask("FTask7",100));
        threadPool.start();
        while (!threadPool.isTerminated()) { }
    }

    public static void scalableThreadPoolDemo(){
        System.out.println("\nScalable Thread Pool Test:");
        MyThreadPool threadPool = new ScalableThreadPool(2, 4);
        threadPool.execute(getTask("STask01",1000));
        threadPool.execute(getTask("STask02",500));
        threadPool.execute(getTask("STask03",200));
        threadPool.execute(getTask("STask04",200));
        threadPool.execute(getTask("STask05",200));
        threadPool.shutdown();

        threadPool.submit(getTask("STask06",100));
        threadPool.submit(getTask("STask07",100));
        threadPool.submit(getTask("STask08",100));
        threadPool.submit(getTask("STask09",100));
        threadPool.submit(getTask("STask010",100));
        threadPool.start();
        while (!threadPool.isTerminated()) { }
    }

    public static void main(String[] args) {
        fixedThreadPoolDemo();
        scalableThreadPoolDemo();
    }
}
