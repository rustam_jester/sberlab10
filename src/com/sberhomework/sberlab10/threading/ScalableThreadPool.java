package com.sberhomework.sberlab10.threading;

import java.util.*;
import java.util.concurrent.*;

public class ScalableThreadPool implements MyThreadPool, ExecutorService {
    private final List<Thread> pool;
    private Thread managingThread;
    private Thread cleaningThread;
    private final Queue<Runnable> taskQueue;

    private final int minSize;
    private final int maxSize;

    private boolean isShutdown;

    public ScalableThreadPool(int minSize, int maxSize) {
        if (minSize <= 0) {
            throw new IllegalArgumentException("MIN Thread count can't be <= 0");
        }
        if (maxSize < minSize){
            throw new IllegalArgumentException("MAX Thread count can't be less than MIN count");
        }
        this.minSize = minSize;
        this.maxSize = maxSize;

        taskQueue = new ConcurrentLinkedQueue<>();
        managingThread = new Thread();
        cleaningThread = new Thread();
        pool = Collections.synchronizedList(new ArrayList<>(minSize));
        for (int i = 0; i < minSize; i++){
            Thread t = new Thread();
            t.setDaemon(true);
            pool.add(t);
        }
    }
    @Override
    public void start() {
        isShutdown = false;
        useManagerThread();
    }

    @Override
    public void execute(Runnable runnable) {
        synchronized (pool){
            int index = getAvailableThreadIndex();
            if (index != -1){
                Thread work = new Thread(runnable);
                work.setDaemon(true);
                pool.set(index, work);
                work.start();
            } else if (pool.size() < maxSize){
                Thread work = new Thread(runnable);
                work.setDaemon(true);
                pool.add(work);
                work.start();
                useCleaningThread();
            } else{
                System.out.println("Threads '"+pool.size()+"' all busy now. Waiting for completion");
                taskQueue.add(runnable);
                useManagerThread();
            }
        }
    }


    @Override
    public void shutdown() {
        isShutdown = true;
        synchronized (pool){ pool.forEach(Thread::interrupt); }
    }

    @Override
    public List<Runnable> shutdownNow() {
        isShutdown = true;
        synchronized (pool){ pool.forEach(Thread::interrupt); }
        List<Runnable> list = new ArrayList<>();
        list.addAll(pool);
        list.addAll(taskQueue);
        return list;
    }

    @Override
    public boolean isShutdown() {
        return isShutdown;
    }

    @Override
    public boolean isTerminated() {
        if (!isShutdown && taskQueue.size() != 0) return false;
        synchronized (pool){
            for (Thread t : pool){
                if (t.isAlive() || t.isInterrupted()){
                    return false;
                }
            }
        }
        return true;
    }

    @Override
    public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
        Thread.sleep(unit.toMillis(timeout));
        return isTerminated();
    }

    @Override
    public <T> Future<T> submit(Callable<T> task) {
        throw new UnsupportedOperationException("submit");
    }

    @Override
    public <T> Future<T> submit(Runnable task, T result) {
        taskQueue.add(task);
        return new FutureTask<>(task, result);
    }

    @Override
    public Future<?> submit(Runnable task) {
        taskQueue.add(task);
        return new FutureTask<Void>(task, null);
    }

    @Override
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) throws InterruptedException {
        throw new UnsupportedOperationException("invokeAll");
    }

    @Override
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit) throws InterruptedException {
        throw new UnsupportedOperationException("invokeAll");
    }

    @Override
    public <T> T invokeAny(Collection<? extends Callable<T>> tasks) throws InterruptedException, ExecutionException {
        throw new UnsupportedOperationException("invokeAny");
    }

    @Override
    public <T> T invokeAny(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        throw new UnsupportedOperationException("invokeAny");
    }

    private void runManaging(){
        while (!taskQueue.isEmpty() && !isShutdown){
            synchronized (pool) {
                int index = getAvailableThreadIndex();
                if (index != -1) {
                    Thread work = new Thread(taskQueue.remove());
                    work.setDaemon(true);
                    pool.set(index, work);
                    work.start();
                } else if (pool.size() < maxSize) {
                    Thread work = new Thread(taskQueue.remove());
                    work.setDaemon(true);
                    pool.add(work);
                    work.start();
                    useCleaningThread();
                }
            }
            Thread.yield();
        }
    }

    private void runCleaning(){
        while (pool.size() > minSize){
            synchronized (pool) {
                ListIterator<Thread> iter = pool.listIterator();
                while (iter.hasNext()) {
                    Thread thread = iter.next();
                    if (pool.size() > minSize && (!thread.isAlive() || thread.isInterrupted())) {
                        iter.remove();
                        System.out.println("Threads returned to: '"+pool.size()+"' count");
                        break;
                    }
                }
            }
            Thread.yield();
        }
    }

    private int getAvailableThreadIndex(){
        for(int i = 0; i < pool.size(); i++){
            if (!pool.get(i).isAlive() || pool.get(i).isInterrupted()){
                return i;
            }
        }
        return -1;
    }

    private void useCleaningThread(){
        if (!cleaningThread.isAlive() || cleaningThread.isInterrupted()){
            cleaningThread = new Thread(this::runCleaning);
            cleaningThread.setDaemon(true);
            cleaningThread.start();
        }
    }

    private void useManagerThread(){
        if (!managingThread.isAlive() || managingThread.isInterrupted()){
            managingThread = new Thread(this::runManaging);
            managingThread.setDaemon(true);
            managingThread.start();
        }
    }
}
