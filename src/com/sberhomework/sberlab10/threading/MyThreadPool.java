package com.sberhomework.sberlab10.threading;

import java.util.List;
import java.util.concurrent.*;

public interface MyThreadPool extends ExecutorService {
    void start();
    void execute(Runnable runnable);

    @Override
    void shutdown();

    @Override
    List<Runnable> shutdownNow();

    @Override
    boolean isShutdown();

    @Override
    boolean isTerminated();

    @Override
    boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException;

    @Override
    <T> Future<T> submit(Runnable task, T result);

    @Override
    Future<?> submit(Runnable task);
}
