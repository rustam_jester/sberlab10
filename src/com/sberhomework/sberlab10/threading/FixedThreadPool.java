package com.sberhomework.sberlab10.threading;

import java.util.*;
import java.util.concurrent.*;

public class FixedThreadPool implements MyThreadPool {
    private final int poolSize;
    private final List<Thread> pool;
    private Thread managingThread;
    private final Queue<Runnable> taskQueue;

    private boolean isShutdown;

    public FixedThreadPool(int poolSize){
        if (poolSize <= 0) {
            throw new IllegalArgumentException("Thread count can't be <= 0");
        }
        managingThread = new Thread();
        taskQueue = new ConcurrentLinkedQueue<>();
        this.poolSize = poolSize;
        pool = new ArrayList<>(poolSize);
        for (int i = 0; i < poolSize; i++){
            Thread t = new Thread();
            t.setDaemon(true);
            pool.add(t);
        }
    }

    @Override
    public void start() {
        isShutdown = false;
        useManagerThread();
    }

    @Override
    public void execute(Runnable runnable) {
        int index = getAvailableThreadIndex();
        if (index != -1){
            Thread work = new Thread(runnable);
            pool.set(index, work);
            work.start();
        } else {
            System.out.println("Threads '"+pool.size()+"' all busy now. Waiting for completion");
            taskQueue.add(runnable);
            useManagerThread();
        }
    }


    @Override
    public void shutdown() {
        isShutdown = true;
        pool.forEach(Thread::interrupt);
    }

    @Override
    public List<Runnable> shutdownNow() {
        isShutdown = true;
        pool.forEach(Thread::interrupt);
        List<Runnable> list = new ArrayList<>();
        list.addAll(pool);
        list.addAll(taskQueue);
        return list;
    }

    @Override
    public boolean isShutdown() {
        return isShutdown;
    }

    @Override
    public boolean isTerminated() {
        if (!isShutdown && taskQueue.size() != 0) return false;
        for (Thread t : pool){
            if (t.isAlive() || t.isInterrupted()){
                return false;
            }
        }
        return true;
    }

    @Override
    public boolean awaitTermination(long timeout, TimeUnit unit) throws InterruptedException {
        Thread.sleep(unit.toMillis(timeout));
        return isTerminated();
    }

    @Override
    public <T> Future<T> submit(Callable<T> task) {
        throw new UnsupportedOperationException("submit");
    }

    @Override
    public <T> Future<T> submit(Runnable task, T result) {
        taskQueue.add(task);
        return new FutureTask<>(task, result);
    }

    @Override
    public Future<?> submit(Runnable task) {
        taskQueue.add(task);
        return new FutureTask<Void>(task, null);
    }

    @Override
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks) throws InterruptedException {
        throw new UnsupportedOperationException("invokeAll");
    }

    @Override
    public <T> List<Future<T>> invokeAll(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit) throws InterruptedException {
        throw new UnsupportedOperationException("invokeAll");
    }

    @Override
    public <T> T invokeAny(Collection<? extends Callable<T>> tasks) throws InterruptedException, ExecutionException {
        throw new UnsupportedOperationException("invokeAny");
    }

    @Override
    public <T> T invokeAny(Collection<? extends Callable<T>> tasks, long timeout, TimeUnit unit) throws InterruptedException, ExecutionException, TimeoutException {
        throw new UnsupportedOperationException("invokeAny");
    }

    private void runManaging(){
        while (!taskQueue.isEmpty() && !isShutdown){
            int index = getAvailableThreadIndex();
            if (index != -1){
                Thread work = new Thread(taskQueue.remove());
                work.setDaemon(true);
                pool.set(index, work);
                work.start();
            }
            Thread.yield();
        }
    }

    private int getAvailableThreadIndex(){
        for(int i = 0; i < poolSize; i++){
            if (!pool.get(i).isAlive() || pool.get(i).isInterrupted()){
                return i;
            }
        }
        return -1;
    }

    private void useManagerThread(){
        if (!managingThread.isAlive() || managingThread.isInterrupted()){
            managingThread = new Thread(this::runManaging);
            managingThread.setDaemon(true);
            managingThread.start();
        }
    }
}
